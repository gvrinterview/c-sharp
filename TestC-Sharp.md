# Questions

1. In which of these situations are interfaces better than abstract classes?

- When you need to define an object type's characteristics, use an interface. When you need to define an object type's capabilities, use an abstract class.
- Interfaces are a legacy of older versions of C#, and are interchangeable with the newer abstract class feature.
- When you need a list of capabilities and data that are classes-agnostic, use an interface. When you need a certain object type to share characteristics, use an abstract class.
- You should use both an interface and an abstract class when defining any complex object.

2. Which statement is true of delegates?

- Delegates are not supported in the current version of C#
- They cannot be used as callbacks.
- Only variables can be passed to delegates as parameters.
- They can be chained together.

3. Which choice best defines C#'s asynchronous programming model?

- reactive
- inherited callback
- task-based
- callback-based

4. How would you determine if a class has a particular attribute?

- A

```cs
var type = typeof(SomeType);
var attribute = type.GetCustomAttribute<SomeAttribute>();
```

- B

```cs
var typeof(MyPresentationModel).Should().BeDecoratedWith<SomeAttribute>();
```

- C

```cs
Attribute.GetCustomAttribute, typeof(SubControllerActionToViewDataAttribute)
```

- D

```cs
Attribute.GetCustomAttribute(parameterInfo, typeof(SubControllerActionToViewDataAttribute))
```

5. What is the difference between the ref and out keywords?

- Variables passed to out specify that the parameter is an output parameter, while ref specifies that a variable may be passed to a function without being initialized.
- Variables passed to ref can be passed to a function without being initialized, while out specifies that the value is a reference value that can be changed inside the calling method.
- Variables passed to out can be passed to a function without being initialized, while ref specifies that the value is a reference value that can be changed inside the calling method.
- Variables passed to ref specify that the parameter is an output parameter, while out specifies that a variable may be passed to a function without being initialized.

6. How could you retrieve information about a class, as well as create an instance at runtime?

- reflection
- serialization
- abstraction
- dependency injection

7. What is this code an example of?
```cs
    private static object objA;
    private static object objB;
    private User user;

    public async void PerformTask() 
    {
        if(user.canB) 
        {
            PerformTaskB();
        } else 
        {
            PerformTaskA();
        }
    }

    private void PerformTaskA()
    {
        lock (objB)
        {
            Thread.Sleep(1000);
            lock (objA) { }
        }
    }

    private void PerformTaskB()
    {
        lock (objA)
        {
            lock (objB) { }
        }
    }
```

- a private class that uses multithreading
- multithread coding
- thread mismanagement
- a potential deadlock

8. What is the difference between an anonymous type and a regular data type?

- Anonymous types don't have type names
- Anonymous types can only be static
- Anonymous types can be used only in structs
- Anonymous types don't work with LINQ.

9. When would you use a Dictionary rather that an Array type in your application?

- when you need a jagged collection structure
- when you need to store values of the same type
- when you need to store key-value pairs rather than single values
- when you need an ordered, searchable list

10. What is the difference between `a.Equals(b)` and `a == b` ?

- The `.Equals` method compares reference identities while the `==` compares contents.
- The `.Equals` method compares primitive values while `==` compares all values.
- The `.Equals` method compares contents while `==` compares references objects identity.
- The `.Equals` method compares reference type while `==` compares primitive value types.

11. Which choice best describes a deadlock situation?

- when you try to instantiate two objects at the same time in the same class or struct
- when you are trying to execute an action after a user event is registered
- when simultaneous instructions are waiting on each other to finish before executing
- when you try to execute a series of events simultaneously on multiple threads

12. Which code snippet declares an anonymous type named userData?

- `var<<!---->T> userData = new <<!---->T> { name = "John", age = 32 };`
- `var userData = new { name = "John", age = 32 };`
- `dynamic userData = new Var { name = "John", age = 32 };`
- `Anonymous<T> userData = new Anonymous<T> { name = "John", age = 32 };` 

13. What does `T` denote in a definition of the type `List<T>`?

- the generic number of elements that will be part of the list
- the generic type of the elements that will be part of the list
- the generic name that the class List can take
- the generic value that each element of the list will take on

14. The `default` operator applied to a reference type returns :

- an instance of type
- the value null
- exception at runtime
- compilation error

15. Which instruction is used to implement an iterator ?

- yield
- enumerator
- foreach
- iterator

16. Which of the following statements is correct about the C#.NET program given below?

```cs
using System;
namespace IndiabixConsoleApplication
{
    class MyProgram
    {
        static void Main(string[] args)
        {
            int index = 6;
            int val = 44;
            int[] a = new int[5];
            try
            {
                a[index] = val ;
            }    
            catch(IndexOutOfRangeException e)
            {
                Console.Write("Index out of bounds ");
            }
            Console.Write("Remaining program");
        }
    }
}
```

- Value 44 will get assigned to a[6].
- It will output: Index out of bounds
- It will output: Remaining program
- It will not produce any output.
- It will output: Index out of bounds Remaining program
	
17. Which of the following statements are correct about exception handling in C#.NET?

A. If our program does not catch an exception then the .NET CLR catches it.

B. It is possible to create user-defined exceptions.

C. All types of exceptions can be caught using the Exception class.

D. CLRExceptions is the base class for all exception classes.

E. For every try block there must be a corresponding finally block.

- A and B only
- A, B and C only
- D and E only
- All of the above
- None of the above

18. Which of the following statement is correct about the C#.NET code snippet given below?

```cs
public class Sample
{
    public int x;
    public virtual void fun()
    { }
}
public class DerivedSample : Sample
{
    new public void fun()
    { }
}
```

- DerivedSample class hides the fun() method of base class.
- The DerivedSample class version of fun() method gets called using Sample class reference which holds DerivedSample class object.
- The code replaces the DerivedSample class version of fun() method with its Sample class version.
- It is not possible to hide Sample class version of fun() method without use of new in DerivedSample class.

19. In which of the following areas are delegates commonly used?

A. Remoting

B. Serialization

C. File Input/Output

D. Multithreading

E. Event handling

- A and B only
- A and E only
- A, B and C only
- D and E only
- All of the above

20. Which of the following statements are correct about the C#.NET program given below?

```cs
namespace IndiabixConsoleApplication
{ 
    class SampleProgram
    { 
        static void Main(string[ ] args)
        { 
            int a = 5;
            int s = 0, c = 0; 
            s, c = fun(a); 
            Console.WriteLine(s +" " + c) ;
        }
        static int fun(int x)
        {
            int ss, cc;
            ss = x * x; cc = x * x * x; 
            return ss, cc;
        } 
    } 
}
```

A. An error will be reported in the statement s, c = fun(a); since multiple values returned from a function cannot be collected in this manner.
B. It will output 25 125.
C. It will output 25 0.
D. It will output 0 125.
E. An error will be reported in the statement return ss, cc; since the function cannot return multiple values.

- A, C
- B, D
- D, E
- A, E
- None of these

21. Which of the following statements is correct about Interfaces used in C#.NET?

- All interfaces are derived from an Object class.
- Interfaces can be inherited.
- All interfaces are derived from an Object interface.
- Interfaces can contain only method declaration.
- Interfaces can contain static data and methods.

22. Which of the following statements is correct?

- When a class inherits an interface it inherits member definitions as well as its implementations.
- An interface cannot contain the signature of an indexer.
- Interfaces members are automatically public.
- To implement an interface member, the corresponding member in the class must be public as well as static.

23. Which of the following statements is correct about the C#.NET code snippet given below?

```cs
enum per
{
    married, 
    unmarried, 
    divorced, 
    spinster
}
per.married = 10; 
Console.WriteLine(per.unmarried);
```

- The program will output a value 11.
- The program will output a value 1.
- The program will output a value 2.
- The program will report an error since an enum element cannot be assigned a value outside the enum declaration.
- The enum elements must be declared private.

24. What is the correct way to write an event named apiResult based on a delegate named ResultCallback?

- public void event ResultCallback apiResult;
- public event ResultCallback(() -> apiResult);
- public event void ResultCallback
- public event ResultCallback apiResult;

25. What method correctly extends the string class?

- public static string IsvalidName(this string i, string value) {}
- public static void IsvalidName(this string i, string value) {}
- public string IsvalidName(this string i, string value) {}
- public void IsvalidName(this string i, string value) {}